import sys, collections, re, math, random, json, os
from PIL import Image
import settings


def check_min_max_filter(value:dict): # If any values are missing from the dict, add them back in. We should always assume these values are present
    value = collections.OrderedDict(value)
    try:
        value["min"]
    except KeyError:
        value.update({"min": 0})
        value.move_to_end("min", False)
    try:
        value["max"]
    except KeyError:
        value.update({"max": sys.maxsize})
        value.move_to_end("max")
    return dict(value)


def set_img_path(data, x, y):
    try:
        return data[x][y].format(settings.IMAGE_STATIC_PATH)
    except AttributeError:
        return None


def set_voice_path(data, x, y, region):
    try:
        if region == "na":
            return data[x][y].format(settings.VOICE_STATIC_PATH_NA)
        else:
            return data[x][y].format(settings.VOICE_STATIC_PATH_JP)
    except AttributeError:
        return None


def eprint(ex:Exception):
    template = "{}: {}"
    print(template.format(type(ex).__name__, ex))


def format_sql_part(format_:list):
    sql = ""
    if format_:
        for item in format_:
            sql += "medal_{}, ".format(item)
    else:
        return "*"
    return sql.lower().rstrip(", ")


def filter_sql_part(filter:dict):
    sql = ""
    if filter:
        for key, value in filter.items():
            if type(value) != dict:
                sql += "LOWER(medal_{}) = LOWER('{}') AND ".format(key, value)
            else:
                value = check_min_max_filter(value)
                sql += "medal_{} >= {} AND medal_{} <= {} AND ".format(key, value["min"], key, value["max"])
        return sql.rstrip(" AND ")
    else:
        return ""


def buff_subsql_part(buff:dict):
    # {"include": [... ], "exclude":[... ]}
    include_sql = "IN (SELECT medal_buff.medal_id FROM medal_buff INNER JOIN buff ON medal_buff.buff_id = buff.buff_id WHERE {})"
    exclude_sql = "NOT IN (SELECT medal_buff.medal_id FROM medal_buff INNER JOIN buff ON medal_buff.buff_id = buff.buff_id WHERE {})"
    exclude_sub_where = ""
    include_sub_where = ""
    for key, sub_dict in buff.items():
        if key == "include":
            for key, value in sub_dict.items():
                if type(value) != dict:
                    include_sub_where += "buff.buff_name = '{}' AND medal_buff.medal_buff_level = {} AND ".format(key, value)
                else:
                    value = check_min_max_filter(value)
                    include_sub_where += "buff.buff_name = '{}' AND medal_buff.medal_buff_level >= {} AND medal_buff.medal_buff_level <= {} AND ".format(key, value["min"], value["max"])
        elif key == "exclude":
            for key, value in sub_dict.items():
                if type(value) != dict:
                    include_sub_where += "buff.buff_name = '{}' AND medal_buff.medal_buff_level = {} AND ".format(key, value)
                else:
                    value = check_min_max_filter(value)
                    include_sub_where += "buff.buff_name = '{}' AND medal_buff.medal_buff_level >= {} AND medal_buff.medal_buff_level <= {} AND ".format(key, value["min"], value["max"])
    include_sub_where = include_sub_where.rstrip(" AND ")
    exclude_sub_where = exclude_sub_where.rstrip(" AND ")
    if include_sub_where and not exclude_sub_where:
        return include_sql.format(include_sub_where)
    elif exclude_sub_where and not include_sub_where:
        return exclude_sql.format(exclude_sub_where)
    elif include_sub_where and exclude_sub_where:
        return include_sql.format(include_sub_where) + " AND medal.medal_id " + exclude_sql.format(exclude_sub_where)
    else:
        return ""


def create_sql_combination(buff_, filter_, format_):
    filter_sql = filter_sql_part(filter_)
    format_sql = format_sql_part(format_)
    if buff_:
        buff_sql = buff_subsql_part(buff_)
    else:
        buff_sql = ""
    sql = "SELECT {} FROM medal".format(format_sql)
    if filter_sql or buff_sql:
        sql += " WHERE "
    if filter_sql:
        sql += filter_sql + " AND "
    if buff_sql:
        sql += "medal.medal_id " + buff_sql + " AND "
    return sql.rstrip(" AND ")


def get_names(db):
    cursor = db.cursor()
    sql = "SELECT DISTINCT medal_name FROM medal"
    cursor.execute(sql)
    data = cursor.fetchall()
    return {"names":[x[0].lower() for x in data if x[0] is not None]}


def get_image_locations(db):
    cursor = db.cursor()
    sql = "SELECT DISTINCT medal_image_link FROM medal"
    cursor.execute(sql)
    return {"images": [x[0].format(settings.IMAGE_STATIC_PATH) for x in cursor.fetchall() if x[0]]}


def update_data_dict(d, data, x, region):
    img_path = set_img_path(data, x, 15)
    if region == "na":
        voice_path = set_voice_path(data, x, 17, "na")
    else:
        voice_path = set_voice_path(data, x, 17, "jp")
    d.update({x: {
        "id": data[x][0],
        "name": data[x][1],
        "rarity": data[x][2],
        "type": data[x][3],
        "element": data[x][4],
        "direction": data[x][5],
        "tier": data[x][6],
        "targets": data[x][7],
        "strength": data[x][8],
        "defence": data[x][9],
        "cost": data[x][10],
        "multiplier": data[x][11],
        "hits": data[x][12],
        "notes": data[x][13],
        "region": data[x][14],
        "pullable": data[x][16],
        "image_link": img_path,
        "voice_link": voice_path
    }})
    return d

#2nd Format for dict data
def update_data_dict_2(data, x, region):
    return_dict = {}
    img_path = set_img_path(data, x, 15)
    if region == "na":
        voice_path = set_voice_path(data, x, 17, "na")
    else:
        voice_path = set_voice_path(data, x, 17, "jp")
    return_dict.update({
        "id": data[x][0],
        "name": data[x][1],
        "rarity": data[x][2],
        "type": data[x][3],
        "element": data[x][4],
        "direction": data[x][5],
        "tier": data[x][6],
        "targets": data[x][7],
        "strength": data[x][8],
        "defence": data[x][9],
        "cost": data[x][10],
        "multiplier": data[x][11],
        "hits": data[x][12],
        "notes": data[x][13],
        "region": data[x][14],
        "pullable": data[x][16],
        "image_link": img_path,
        "voice_link": voice_path
    })
    return return_dict


def get_medal(db, name=None, format=None, filter=None, buff=None):

    # Apply sanatisation of all inputs
    name = sanatise(name)
    format = sanatise(format)
    filter = sanatise(filter)
    buff = sanatise(buff)

    return_dict = {}
    d = {}
    cursor = db.cursor()
    if name and not format and not filter and not buff:
        #Only using filter
        sql = "SELECT * FROM medal WHERE LOWER(medal_name) = LOWER(?)"
        cursor.execute(sql, (name,))
        data = cursor.fetchall()
        for x in range(len(data)):
            d = update_data_dict(d, data, x, "na")
    else:
        sql = create_sql_combination(buff, filter, format)
        try:
            cursor.execute(sql)
        except Exception as e:
            eprint(e)
            return {"error": "Failed to execute script"}
        data = [[d for d in x] for x in cursor.fetchall()] # Convert data into a mutable list so we can change the values if needed
        #Apply voice link and image link formatting if needed
        try:
            if format: # Added a check to make sure format exists first
                if "image_link" in format:
                    index = format.index("image_link")
                    for value in data:
                        value[index] = value[index].format(settings.IMAGE_STATIC_PATH)
                if "voice_link" in format:
                    index = format.index("voice_link")
                    try:
                        if filter["region"] == "na":
                            for value in data:
                                if value[index]:
                                    value[index] = value[index].format(settings.VOICE_STATIC_PATH_NA)
                        else:
                            for value in data:
                                if value[index]:
                                    value[index] = value[index].format(settings.VOICE_STATIC_PATH_JP)
                    except KeyError:
                        for value in data:
                            if value[index]:
                                value[index] = value[index].format(settings.VOICE_STATIC_PATH_JP)
        except TypeError as e:
            eprint(e)
        except Exception as e:
            eprint(e)
            return {"error": "Failed to format links"}
        for i in range(len(data)): # Compile the data into a usable dict format to be sent as json
            inner = {}
            if format:
                for j in range(len(format)):
                    inner.update({format[j]:data[i][j]})
                d.update({i:inner})
            else:
                d = update_data_dict(d, data, i, "jp")
    if d:
        return_dict.update({"medal":d})
    else:
        return {"error":"No medal found"}
    return return_dict


def get_buffs(db):
    cursor = db.cursor()
    sql = "SELECT buff_name, buff_description FROM buff;"
    cursor.execute(sql)
    return ["{} - {}".format(x[0], x[1]) for x in cursor.fetchall()]

def get_medal_2(db, format, filter, buff):
    sql = create_sql_combination(buff, filter, format)
    cursor = db.cursor()
    cursor.execute(sql)
    return_list = []
    data = [[d for d in x] for x in cursor.fetchall()]  # Convert data into a mutable list so we can change the values if needed
    if not data:
        return {"error": "No medals found"}
    if not format:
        if filter.get("region"):
            for x in range(len(data)):
                return_list.append(update_data_dict_2(data, x, filter.get("region").upper()))
        else:
            for x in range(len(data)):
                return_list.append(update_data_dict_2(data, x, "na"))
    else:
        for i in range(len(data)):
            inner = {}
            for j in range(len(format)):
                if format[j] == "image_link":
                    inner.update({format[j]: data[i][j].format(settings.IMAGE_STATIC_PATH)})
                elif format[j] == "voice_link" and data[i][j]:
                    if filter.get("region"):
                        inner.update({format[j]: data[i][j].format(settings.VOICE_STATIC_PATH.format(filter.get("region").upper()))})
                    else:
                        inner.update({format[j]: data[i][j].format(settings.VOICE_STATIC_PATH.format("JP"))})
                else:
                    inner.update({format[j]: data[i][j]})
            return_list.append(inner)
    return return_list


def create_spritesheet(image_list:list, name): # List of tuples ["link", "link2"]
    return_data = []
    for i in range(100): # A very big number, Since i is squared and that value is the maximum number of images which can be stored on the spritesheet
        if i**2 >= len(image_list):
            dimension = i
            break
    reference_image = Image.open("static/temp/"+image_list[0])
    box = (dimension * reference_image.width, dimension * reference_image.height)
    new = Image.new("RGBA", box)
    for i in range(len(image_list)):
        x_offset = i%dimension*reference_image.width #Width
        y_offset = math.trunc(i/dimension)*reference_image.height #Height
        new.alpha_composite(Image.open("static/temp/"+image_list[i]), (x_offset, y_offset))
        return_data.append({"image":image_list[i],"x_offset":x_offset,"y_offset":y_offset})
    new.save("static/images/spritesheets/{}.png".format(name))
    return {"data":return_data, "spritesheet_location":"static/images/spritesheets/{}.png".format(name), "width":reference_image.width, "height":reference_image.height}


def generate_spritesheet(db, filter, buff, name):
    sql = create_sql_combination(buff, filter, ["image_link"])
    cursor = db.cursor()
    cursor.execute(sql)
    data = cursor.fetchall()
    return_data = []
    image_list = [re.sub(r"{}/", "", x[0]) for x in data]
    image_list = list(chunks(image_list, settings.SPRITESHEET_MAX_IMAGES))

    for x in range(len(image_list)):
        return_data.append(create_spritesheet(image_list[x], name+"_{}".format(x)))
    json.dump(return_data, open(os.path.join("static", "images", "spritesheets", name+".json"), "w"))

    return return_data


def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]


def sanatise(object):
    # Recursivley sanatise the object (Will always assume either dict, list int, string or tuple
    if isinstance(object, str):
        # Removes bad words
        return re.sub(r"[^\w#\[\]\.&\(\) {\}]", "", object)
    elif isinstance(object, list):
        return [sanatise(x) for x in object] #Assumes a list/dict of (lists, dicts, ints or strings) and calls sanatise on all elements
    elif isinstance(object, dict):
        return {sanatise(key):sanatise(value) for key, value in object.items()}
    elif isinstance(object, tuple):
        return (sanatise(x) for x in object)
    elif isinstance(object, int) or isinstance(object, bool):
        return object
    else:
        return None

def gen_random_id(size):
    string = ""
    for x in range(size):
        string += random.choice("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ12345678901234567890")
    return string
